/**
 * @file   Point.h
 * @author Wang Heyu <wang@wukong>
 * @date   Tue May  4 21:44:13 2021
 * 
 * @brief  A point in a space, with an index.
 * 
 * 
 */


#ifndef __CRAZYFISH_POINT__
#define __CRAZYFISH_POINT__

#include "Geometry.h"
#include <initializer_list>


#define TEMPLATE template <size_t DIM, typename T>

TEMPLATE class Point;

TEMPLATE
class Point : public std::valarray<T>, public Geometry
{
public:
    Point();       /**< Default constructor. */
    Point(const std::initializer_list<T> _com_list);
};

TEMPLATE
Point<DIM, T>::Point() : std::valarray<T>(DIM), Geometry()
{
    vertex.resize(1);
    boundary.resize(1);
    neighbor.resize(0);
    dofs.resize(1);
};

TEMPLATE
Point<DIM, T>::Point(const std::initializer_list<T> _com_list) : std::valarray<T>(_com_list), Geometry()
{
};

#undef TEMPLATE
#else
// DO NOTHING.
#endif

