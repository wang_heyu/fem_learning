#include "Matrix.h"
#include <iostream>

int main(int argc, char *argv[])
{
    Matrix<double> A(3, 3);
    A(0, 0) = 1.0;
    A(0, 1) = 2.0;
    A(0, 2) = 3.0;
    A(1, 0) = 4.0;
    A(1, 1) = 5.0;
    A(1, 2) = 6.0;
    A(2, 0) = 7.0;
    A(2, 1) = 8.0;
    A(2, 2) = 9.0;
    for (size_t i = 0; i < 3; i++)
    {
	for (size_t j = 0; j < 3; j++)
	    std::cout << A(i, j) << "\t";
	std::cout << std::endl;
    }
    std::slice myslice=std::slice(3,3,1);

    std::valarray<double> p({1, 2, 3});
    p = A * p;
    for (size_t i = 0; i < 3; i++)
	std::cout << p[i] << "\t";
    std::cout << std::endl;
    
    Matrix<double> B(3, 3);
    B=A.transpose();
    for (size_t i = 0; i < 3; i++)
    {
	for (size_t j = 0; j < 3; j++)
	    std::cout << B(i, j) << "\t";
	std::cout << std::endl;
    }

    A=A.transpose();
    for (size_t i = 0; i < 3; i++)
    {
	for (size_t j = 0; j < 3; j++)
	    std::cout << B(i, j) << "\t";
	std::cout << std::endl;
    }
    return 0;
};
