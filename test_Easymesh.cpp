#include "Mesh.h"
#include <iostream>
#include <string>

int main(int argc, char* argv[])
{
    typedef Point<2, double> pnt_t;
    Easymesh mesh;
    std::string mesh_file = argv[1];
    mesh.readData(mesh_file);
	std::cout << "points / vertexes list:" << std::endl;
    for (size_t i = 0; i < mesh.get_n_points(); i++)
    {
	int idx_p = mesh.get_point(i).get_index();
	const pnt_t &pnt = mesh.get_point(idx_p); 
	std::cout << idx_p << ": (" << pnt[0] << ", " << pnt[1] << ")" << std::endl;
	
    }
    std::cout << "sides list:" << std::endl;
    for (size_t i = 0; i < mesh.get_n_edges(); i++)
    {
	size_t idx_s = mesh.get_edge(i).get_index();
	const Geometry &si = mesh.get_edge(idx_s);
	size_t n_vtx = si.get_n_vertex();
	size_t n_nei = si.get_n_neighbor();
	std::cout << idx_s << ": ";
	for (size_t j = 0; j < n_vtx; j++)
	{
	    size_t idx_p = si.get_vertex(j);
	    const pnt_t &pnt = mesh.get_point(idx_p); 
	    std::cout << idx_p << ": (" << pnt[0] << ", " << pnt[1] << "), ";
	}
	std::cout << "\b\b" << std::endl;
    }
    for (size_t i = 0; i < mesh.get_n_grids(); i++)
    {
	int idx_g = mesh.get_grid(i).get_index();
	const Geometry &gd = mesh.get_grid(idx_g);
	size_t n_vtx = gd.get_n_vertex();
	size_t n_bnd = gd.get_n_boundary();
	std::cout << idx_g << ": ";
	for (size_t j = 0; j < n_vtx; j++)
	{
	    size_t idx_p = gd.get_vertex(j);
	    const pnt_t &pnt = mesh.get_point(idx_p); 
	    std::cout << idx_p << ": (" << pnt[0] << ", " << pnt[1] << "), ";
	}
	std::cout << "\b\b" << std::endl;
    }
    return 0;
};
