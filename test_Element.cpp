#include <iostream>
#include "Element.h"

int main(int argc, char* argv[])
{
    P1Element<double> p1;
    
    Point<2, double> xy0({-0.5, -0.5});      // x_1
    Point<2, double> xy1({3.14, 0.0});       // x_2
    Point<2, double> xy2({0.0, 3.15});      // x_3
    p1.set_global_dof(0, xy0);
    p1.set_global_dof(1, xy1);
    p1.set_global_dof(2, xy2);
    Point<2, double> z({2.36395, -0.686518});
    
    std::cout << p1.local2global(z)[0] << ", " << p1.local2global(z)[1] << std::endl;
    std::cout << p1.basis_function(1, z) << std::endl;
    p1.read_quad_info("data/triangle.tmp_geo", 1);
    size_t n_q_pnts = p1.get_n_quad_points();
    std::cout << n_q_pnts << std::endl;
    const std::valarray<Point<2, double> >& q_pnts = p1.get_quad_points();
    const std::valarray<double>& q_weis = p1.get_quad_weights();
    for (size_t i = 0; i < n_q_pnts; i++)
	std::cout << "(" << q_pnts[i][0] << ", " << q_pnts[i][1] << "):" << q_weis[i] << std::endl;
    return 0;
};
