# Project 1 用 P1 单元完成有限元设计

目标问题：
$$
-\Delta u = 2 \sin x \sin y, \\
\left. u\right|_b = \sin x \sin y, x \in [1, 2] \times [1, 2].
$$
请用P1有限元计算上述问题。

1. 可以用规则或不规则网格;
2. 请用C++设计完成，代码上传至你个人的gitee.com/fem2021项目;
3. 请给出问题的误差阶，并用数值结果验证; 
4. 请提交一个文件 report.tex至 fem2021，作为数值结果的报告，中文tex建议使用ctex或者xelatex；
5. 网格剖分允许使用 easymesh，线性计算允许使用 BLAS，LAPACK或者 eigen;
6. 绘图输出允许使用 matlab;
7. （选做）：使用多重网格完成数值求解;
8. （选做）：完成 P2 有限元计算;
9. 同时完成7和8的只要结果正确，项目作业必然满分;
10. （选做）：为https://gitee.com/wang_heyu/fem2021项目作出实质贡献的，根据贡献情况有加分。